﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using HotShotAccountingServices.Entities;
using Microsoft.Extensions.Configuration;

namespace HotShotAccountingServices
{
    public class Startup
    {


        public IConfigurationRoot _config;
        public IHostingEnvironment _env { get; set; }

        public Startup(IHostingEnvironment env)
        {

            _env = env;

            var builder = new ConfigurationBuilder()
                .SetBasePath(_env.ContentRootPath)
                .AddJsonFile("config.json")
                .AddEnvironmentVariables();

            _config = builder.Build();

        }


        

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddSingleton(_config);

            services.AddDbContext<DataContext>();
            services.AddScoped<DataRepository>();
            services.AddMvc().AddJsonOptions(options => {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsEnvironment("Development"))
            {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();

            } else {

                app.UseExceptionHandler("/Error/ServerError");
                app.UseStatusCodePagesWithReExecute("/Error/WebError/{0}");

            }


            

            app.UseStaticFiles();
            app.UseMvc(config =>
            {
                config.MapRoute(
                    name: "Default",
                    template: "{controller}/{action}/{id?}",
                    defaults: new { controller = "Main", action = "Index" }
                    );
            });


        }
    }
}
