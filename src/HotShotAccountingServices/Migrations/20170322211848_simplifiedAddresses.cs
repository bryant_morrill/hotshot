﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HotShotAccountingServices.Migrations
{
    public partial class simplifiedAddresses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "dropoff_address_city",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "dropoff_address_country",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "dropoff_address_line_1",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "dropoff_address_line_2",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "dropoff_address_state",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "dropoff_address_zip",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "pickup_address_city",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "pickup_address_country",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "pickup_address_line_1",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "pickup_address_line_2",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "pickup_address_state",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "pickup_address_zip",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "dropoff_address_city",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "dropoff_address_country",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "dropoff_address_line_1",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "dropoff_address_line_2",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "dropoff_address_state",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "dropoff_address_zip",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "pickup_address_city",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "pickup_address_country",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "pickup_address_line_1",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "pickup_address_line_2",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "pickup_address_state",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "pickup_address_zip",
                table: "Contracts");

            migrationBuilder.AddColumn<string>(
                name: "dropoff_address",
                table: "ContractFormStates",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pickup_address",
                table: "ContractFormStates",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "dropoff_address",
                table: "Contracts",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pickup_address",
                table: "Contracts",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "dropoff_address",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "pickup_address",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "dropoff_address",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "pickup_address",
                table: "Contracts");

            migrationBuilder.AddColumn<string>(
                name: "dropoff_address_city",
                table: "ContractFormStates",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "dropoff_address_country",
                table: "ContractFormStates",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "dropoff_address_line_1",
                table: "ContractFormStates",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "dropoff_address_line_2",
                table: "ContractFormStates",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "dropoff_address_state",
                table: "ContractFormStates",
                maxLength: 25,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "dropoff_address_zip",
                table: "ContractFormStates",
                maxLength: 5,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pickup_address_city",
                table: "ContractFormStates",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pickup_address_country",
                table: "ContractFormStates",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pickup_address_line_1",
                table: "ContractFormStates",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pickup_address_line_2",
                table: "ContractFormStates",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pickup_address_state",
                table: "ContractFormStates",
                maxLength: 25,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pickup_address_zip",
                table: "ContractFormStates",
                maxLength: 5,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "dropoff_address_city",
                table: "Contracts",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "dropoff_address_country",
                table: "Contracts",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "dropoff_address_line_1",
                table: "Contracts",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "dropoff_address_line_2",
                table: "Contracts",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "dropoff_address_state",
                table: "Contracts",
                maxLength: 25,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "dropoff_address_zip",
                table: "Contracts",
                maxLength: 5,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pickup_address_city",
                table: "Contracts",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pickup_address_country",
                table: "Contracts",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pickup_address_line_1",
                table: "Contracts",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pickup_address_line_2",
                table: "Contracts",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pickup_address_state",
                table: "Contracts",
                maxLength: 25,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "pickup_address_zip",
                table: "Contracts",
                maxLength: 5,
                nullable: true);
        }
    }
}
