﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HotShotAccountingServices.Migrations
{
    public partial class _20170219 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "pickup_start_form",
                table: "ContractFormStates",
                newName: "pickup_start");

            migrationBuilder.RenameColumn(
                name: "pickup_end_form",
                table: "ContractFormStates",
                newName: "pickup_end");

            migrationBuilder.RenameColumn(
                name: "dropoff_start_form",
                table: "ContractFormStates",
                newName: "dropoff_start");

            migrationBuilder.RenameColumn(
                name: "dropoff_end_form",
                table: "ContractFormStates",
                newName: "dropoff_end");

            migrationBuilder.RenameColumn(
                name: "dispatch_date_form",
                table: "ContractFormStates",
                newName: "dispatch_date");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "pickup_start",
                table: "ContractFormStates",
                newName: "pickup_start_form");

            migrationBuilder.RenameColumn(
                name: "pickup_end",
                table: "ContractFormStates",
                newName: "pickup_end_form");

            migrationBuilder.RenameColumn(
                name: "dropoff_start",
                table: "ContractFormStates",
                newName: "dropoff_start_form");

            migrationBuilder.RenameColumn(
                name: "dropoff_end",
                table: "ContractFormStates",
                newName: "dropoff_end_form");

            migrationBuilder.RenameColumn(
                name: "dispatch_date",
                table: "ContractFormStates",
                newName: "dispatch_date_form");
        }
    }
}
