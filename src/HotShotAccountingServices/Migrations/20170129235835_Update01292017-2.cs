﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HotShotAccountingServices.Migrations
{
    public partial class Update012920172 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "pickup_start",
                table: "Contracts",
                type: "date",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "pickup_end",
                table: "Contracts",
                type: "date",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "pickup_date",
                table: "Contracts",
                type: "date",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "payment_date",
                table: "Contracts",
                type: "date",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "invoice_date",
                table: "Contracts",
                type: "date",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "dropoff_start",
                table: "Contracts",
                type: "date",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "dropoff_end",
                table: "Contracts",
                type: "date",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "dropoff_date",
                table: "Contracts",
                type: "date",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "pickup_start",
                table: "Contracts",
                type: "date",
                nullable: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "pickup_end",
                table: "Contracts",
                type: "date",
                nullable: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "pickup_date",
                table: "Contracts",
                type: "date",
                nullable: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "payment_date",
                table: "Contracts",
                type: "date",
                nullable: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "invoice_date",
                table: "Contracts",
                type: "date",
                nullable: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "dropoff_start",
                table: "Contracts",
                type: "date",
                nullable: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "dropoff_end",
                table: "Contracts",
                type: "date",
                nullable: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "dropoff_date",
                table: "Contracts",
                type: "date",
                nullable: false);
        }
    }
}
