﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HotShotAccountingServices.Migrations
{
    public partial class Update021220175 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "dropoff_date",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "dropoff_end",
                table: "ContractFormStates");

            migrationBuilder.RenameColumn(
                name: "pickup_start",
                table: "ContractFormStates",
                newName: "pickup_start_temp");

            migrationBuilder.RenameColumn(
                name: "pickup_end",
                table: "ContractFormStates",
                newName: "pickup_end_temp");

            migrationBuilder.RenameColumn(
                name: "pickup_date",
                table: "ContractFormStates",
                newName: "dropoff_start_temp");

            migrationBuilder.RenameColumn(
                name: "dropoff_start",
                table: "ContractFormStates",
                newName: "dropoff_end_temp");

            migrationBuilder.RenameColumn(
                name: "dispatch_date",
                table: "ContractFormStates",
                newName: "dispatch_date_temp");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "pickup_start_temp",
                table: "ContractFormStates",
                newName: "pickup_start");

            migrationBuilder.RenameColumn(
                name: "pickup_end_temp",
                table: "ContractFormStates",
                newName: "pickup_end");

            migrationBuilder.RenameColumn(
                name: "dropoff_start_temp",
                table: "ContractFormStates",
                newName: "pickup_date");

            migrationBuilder.RenameColumn(
                name: "dropoff_end_temp",
                table: "ContractFormStates",
                newName: "dropoff_start");

            migrationBuilder.RenameColumn(
                name: "dispatch_date_temp",
                table: "ContractFormStates",
                newName: "dispatch_date");

            migrationBuilder.AddColumn<DateTime>(
                name: "dropoff_date",
                table: "ContractFormStates",
                type: "date",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "dropoff_end",
                table: "ContractFormStates",
                type: "date",
                nullable: true);
        }
    }
}
