﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HotShotAccountingServices.Migrations
{
    public partial class Update021220176 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "pickup_start_temp",
                table: "ContractFormStates",
                newName: "pickup_start_form");

            migrationBuilder.RenameColumn(
                name: "pickup_end_temp",
                table: "ContractFormStates",
                newName: "pickup_end_form");

            migrationBuilder.RenameColumn(
                name: "dropoff_start_temp",
                table: "ContractFormStates",
                newName: "dropoff_start_form");

            migrationBuilder.RenameColumn(
                name: "dropoff_end_temp",
                table: "ContractFormStates",
                newName: "dropoff_end_form");

            migrationBuilder.RenameColumn(
                name: "dispatch_date_temp",
                table: "ContractFormStates",
                newName: "dispatch_date_form");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "pickup_start_form",
                table: "ContractFormStates",
                newName: "pickup_start_temp");

            migrationBuilder.RenameColumn(
                name: "pickup_end_form",
                table: "ContractFormStates",
                newName: "pickup_end_temp");

            migrationBuilder.RenameColumn(
                name: "dropoff_start_form",
                table: "ContractFormStates",
                newName: "dropoff_start_temp");

            migrationBuilder.RenameColumn(
                name: "dropoff_end_form",
                table: "ContractFormStates",
                newName: "dropoff_end_temp");

            migrationBuilder.RenameColumn(
                name: "dispatch_date_form",
                table: "ContractFormStates",
                newName: "dispatch_date_temp");
        }
    }
}
