﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace HotShotAccountingServices.Migrations
{
    public partial class Trips : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "deleted",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "BrokerFormStates");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "Brokers");

            migrationBuilder.AddColumn<int>(
                name: "trip_id",
                table: "Contracts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Trips",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    destination = table.Column<string>(maxLength: 100, nullable: true),
                    notes = table.Column<string>(maxLength: 100, nullable: true),
                    origin = table.Column<string>(maxLength: 100, nullable: true),
                    trip_number = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trips", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_trip_id",
                table: "Contracts",
                column: "trip_id");

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_Trips_trip_id",
                table: "Contracts",
                column: "trip_id",
                principalTable: "Trips",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_Trips_trip_id",
                table: "Contracts");

            migrationBuilder.DropTable(
                name: "Trips");

            migrationBuilder.DropIndex(
                name: "IX_Contracts_trip_id",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "trip_id",
                table: "Contracts");

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "ContractFormStates",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "Contracts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "BrokerFormStates",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "Brokers",
                nullable: false,
                defaultValue: false);
        }
    }
}
