﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HotShotAccountingServices.Migrations
{
    public partial class deletedAndBrokerFee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "broker_fee",
                table: "ContractFormStates",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "ContractFormStates",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "paid",
                table: "ContractFormStates",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "broker_fee",
                table: "Contracts",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "Contracts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "BrokerFormStates",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "deleted",
                table: "Brokers",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "broker_fee",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "paid",
                table: "ContractFormStates");

            migrationBuilder.DropColumn(
                name: "broker_fee",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "BrokerFormStates");

            migrationBuilder.DropColumn(
                name: "deleted",
                table: "Brokers");
        }
    }
}
