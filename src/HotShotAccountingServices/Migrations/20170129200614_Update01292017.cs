﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HotShotAccountingServices.Migrations
{
    public partial class Update01292017 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "status",
                table: "Contracts",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "registration_required",
                table: "Brokers",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "extra_paperwork",
                table: "Brokers",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "certificate_required",
                table: "Brokers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "status",
                table: "Contracts",
                nullable: false);

            migrationBuilder.AlterColumn<bool>(
                name: "registration_required",
                table: "Brokers",
                nullable: false);

            migrationBuilder.AlterColumn<bool>(
                name: "extra_paperwork",
                table: "Brokers",
                nullable: false);

            migrationBuilder.AlterColumn<bool>(
                name: "certificate_required",
                table: "Brokers",
                nullable: false);
        }
    }
}
