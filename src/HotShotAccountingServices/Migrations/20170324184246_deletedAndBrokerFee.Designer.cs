﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using HotShotAccountingServices.Entities;

namespace HotShotAccountingServices.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20170324184246_deletedAndBrokerFee")]
    partial class deletedAndBrokerFee
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("HotShotAccountingServices.Entities.Broker", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("bol_type")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("certificate_required");

                    b.Property<bool>("deleted");

                    b.Property<string>("dispatch_contact_notes")
                        .HasColumnType("text");

                    b.Property<string>("email")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("extra_paperwork");

                    b.Property<string>("fax")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("hours")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("load_board_url")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("login")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("password")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("payment_method")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("phone")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("registration_required");

                    b.Property<string>("update_contact_notes")
                        .HasColumnType("text");

                    b.Property<string>("url")
                        .HasAnnotation("MaxLength", 100);

                    b.HasKey("id");

                    b.ToTable("Brokers");
                });

            modelBuilder.Entity("HotShotAccountingServices.Entities.BrokerFormState", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("active_section");

                    b.Property<string>("bol_type")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("certificate_required");

                    b.Property<bool>("deleted");

                    b.Property<string>("dispatch_contact_notes")
                        .HasColumnType("text");

                    b.Property<string>("email")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("extra_paperwork");

                    b.Property<string>("fax")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("hours")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("load_board_url")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("login")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("name")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("password")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("payment_method")
                        .HasAnnotation("MaxLength", 100);

                    b.Property<string>("phone")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("registration_required");

                    b.Property<string>("update_contact_notes")
                        .HasColumnType("text");

                    b.Property<string>("url")
                        .HasAnnotation("MaxLength", 100);

                    b.HasKey("id");

                    b.ToTable("BrokerFormStates");
                });

            modelBuilder.Entity("HotShotAccountingServices.Entities.Contract", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("bol_type")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<decimal>("broker_fee");

                    b.Property<int>("broker_id");

                    b.Property<bool>("deleted");

                    b.Property<string>("dispatch_contact_notes")
                        .HasColumnType("text");

                    b.Property<DateTime>("dispatch_date")
                        .HasColumnType("date");

                    b.Property<string>("dropoff_address")
                        .HasColumnType("text");

                    b.Property<DateTime?>("dropoff_date")
                        .HasColumnType("date");

                    b.Property<string>("dropoff_email")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<DateTime?>("dropoff_end")
                        .HasColumnType("date");

                    b.Property<string>("dropoff_name")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("dropoff_phone")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<DateTime?>("dropoff_start")
                        .HasColumnType("date");

                    b.Property<DateTime?>("invoice_date")
                        .HasColumnType("date");

                    b.Property<string>("make")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("model")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("order_number")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<bool>("paid");

                    b.Property<DateTime?>("payment_date")
                        .HasColumnType("date");

                    b.Property<string>("payment_method")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<int>("payment_terms");

                    b.Property<string>("pickup_address")
                        .HasColumnType("text");

                    b.Property<DateTime?>("pickup_date")
                        .HasColumnType("date");

                    b.Property<string>("pickup_email")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<DateTime?>("pickup_end")
                        .HasColumnType("date");

                    b.Property<string>("pickup_name")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("pickup_phone")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<DateTime?>("pickup_start")
                        .HasColumnType("date");

                    b.Property<decimal>("price");

                    b.Property<bool>("rated");

                    b.Property<string>("status");

                    b.Property<int?>("trip_number");

                    b.Property<string>("update_contact_notes")
                        .HasColumnType("text");

                    b.Property<string>("vin_number")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("year")
                        .HasAnnotation("MaxLength", 4);

                    b.HasKey("id");

                    b.HasIndex("broker_id");

                    b.ToTable("Contracts");
                });

            modelBuilder.Entity("HotShotAccountingServices.Entities.ContractFormState", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("active_section");

                    b.Property<string>("bol_type")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<decimal>("broker_fee");

                    b.Property<int?>("broker_id");

                    b.Property<string>("broker_name");

                    b.Property<bool>("deleted");

                    b.Property<string>("dispatch_contact_notes")
                        .HasColumnType("text");

                    b.Property<DateTime>("dispatch_date")
                        .HasColumnType("date");

                    b.Property<string>("dropoff_address")
                        .HasColumnType("text");

                    b.Property<string>("dropoff_email")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<DateTime?>("dropoff_end")
                        .HasColumnType("date");

                    b.Property<string>("dropoff_name")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("dropoff_phone")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<DateTime?>("dropoff_start")
                        .HasColumnType("date");

                    b.Property<DateTime?>("invoice_date")
                        .HasColumnType("date");

                    b.Property<string>("make")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("model")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("order_number")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<bool>("paid");

                    b.Property<DateTime?>("payment_date")
                        .HasColumnType("date");

                    b.Property<string>("payment_method")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<int>("payment_terms");

                    b.Property<string>("pickup_address")
                        .HasColumnType("text");

                    b.Property<string>("pickup_email")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<DateTime?>("pickup_end")
                        .HasColumnType("date");

                    b.Property<string>("pickup_name")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("pickup_phone")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<DateTime?>("pickup_start")
                        .HasColumnType("date");

                    b.Property<decimal>("price");

                    b.Property<bool>("rated");

                    b.Property<string>("status");

                    b.Property<int?>("trip_number");

                    b.Property<string>("update_contact_notes")
                        .HasColumnType("text");

                    b.Property<string>("vin_number")
                        .HasAnnotation("MaxLength", 50);

                    b.Property<string>("year")
                        .HasAnnotation("MaxLength", 4);

                    b.HasKey("id");

                    b.ToTable("ContractFormStates");
                });

            modelBuilder.Entity("HotShotAccountingServices.Entities.Contract", b =>
                {
                    b.HasOne("HotShotAccountingServices.Entities.Broker", "broker")
                        .WithMany("contracts")
                        .HasForeignKey("broker_id")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
