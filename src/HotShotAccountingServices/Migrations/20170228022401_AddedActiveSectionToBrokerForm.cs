﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HotShotAccountingServices.Migrations
{
    public partial class AddedActiveSectionToBrokerForm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_BrokerFormState",
                table: "BrokerFormState");

            migrationBuilder.RenameTable(
                name: "BrokerFormState",
                newName: "BrokerFormStates");

            migrationBuilder.AddColumn<int>(
                name: "active_section",
                table: "BrokerFormStates",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_BrokerFormStates",
                table: "BrokerFormStates",
                column: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_BrokerFormStates",
                table: "BrokerFormStates");

            migrationBuilder.DropColumn(
                name: "active_section",
                table: "BrokerFormStates");

            migrationBuilder.RenameTable(
                name: "BrokerFormStates",
                newName: "BrokerFormState");

            migrationBuilder.AddPrimaryKey(
                name: "PK_BrokerFormState",
                table: "BrokerFormState",
                column: "id");
        }
    }
}
