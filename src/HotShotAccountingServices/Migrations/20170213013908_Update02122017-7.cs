﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HotShotAccountingServices.Migrations
{
    public partial class Update021220177 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "broker_id",
                table: "ContractFormStates",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "broker_name",
                table: "ContractFormStates",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "broker_name",
                table: "ContractFormStates");

            migrationBuilder.AlterColumn<int>(
                name: "broker_id",
                table: "ContractFormStates",
                nullable: false);
        }
    }
}
