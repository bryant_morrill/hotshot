﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HotShotAccountingServices.Migrations
{
    public partial class fixedTripFKinFormState : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContractFormStates_Trips_trip_id",
                table: "ContractFormStates");

            migrationBuilder.DropIndex(
                name: "IX_ContractFormStates_trip_id",
                table: "ContractFormStates");

            migrationBuilder.AddColumn<int>(
                name: "trip_number",
                table: "ContractFormStates",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "trip_number",
                table: "ContractFormStates");

            migrationBuilder.CreateIndex(
                name: "IX_ContractFormStates_trip_id",
                table: "ContractFormStates",
                column: "trip_id");

            migrationBuilder.AddForeignKey(
                name: "FK_ContractFormStates_Trips_trip_id",
                table: "ContractFormStates",
                column: "trip_id",
                principalTable: "Trips",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
