﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HotShotAccountingServices.Migrations
{
    public partial class Update021220174 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_BrokerFormState_BrokerFormStateid",
                table: "Contracts");

            migrationBuilder.DropForeignKey(
                name: "FK_ContractFormStates_Brokers_broker_id",
                table: "ContractFormStates");

            migrationBuilder.DropIndex(
                name: "IX_ContractFormStates_broker_id",
                table: "ContractFormStates");

            migrationBuilder.DropIndex(
                name: "IX_Contracts_BrokerFormStateid",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "BrokerFormStateid",
                table: "Contracts");

            migrationBuilder.AlterColumn<int>(
                name: "trip_number",
                table: "ContractFormStates",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "trip_number",
                table: "Contracts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "trip_number",
                table: "ContractFormStates",
                nullable: false);

            migrationBuilder.AlterColumn<int>(
                name: "trip_number",
                table: "Contracts",
                nullable: false);

            migrationBuilder.AddColumn<int>(
                name: "BrokerFormStateid",
                table: "Contracts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ContractFormStates_broker_id",
                table: "ContractFormStates",
                column: "broker_id");

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_BrokerFormStateid",
                table: "Contracts",
                column: "BrokerFormStateid");

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_BrokerFormState_BrokerFormStateid",
                table: "Contracts",
                column: "BrokerFormStateid",
                principalTable: "BrokerFormState",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractFormStates_Brokers_broker_id",
                table: "ContractFormStates",
                column: "broker_id",
                principalTable: "Brokers",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
