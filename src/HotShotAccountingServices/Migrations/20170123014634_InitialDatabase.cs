﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace HotShotAccountingServices.Migrations
{
    public partial class InitialDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Brokers",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    bol_type = table.Column<string>(maxLength: 50, nullable: true),
                    certificate_required = table.Column<bool>(nullable: false),
                    dispatch_contact_notes = table.Column<string>(type: "text", nullable: true),
                    email = table.Column<string>(maxLength: 50, nullable: true),
                    extra_paperwork = table.Column<bool>(nullable: false),
                    fax = table.Column<string>(maxLength: 50, nullable: true),
                    hours = table.Column<string>(maxLength: 100, nullable: true),
                    load_board_url = table.Column<string>(maxLength: 100, nullable: true),
                    login = table.Column<string>(maxLength: 100, nullable: true),
                    name = table.Column<string>(maxLength: 100, nullable: true),
                    password = table.Column<string>(maxLength: 100, nullable: true),
                    payment_method = table.Column<string>(maxLength: 100, nullable: true),
                    phone = table.Column<string>(maxLength: 50, nullable: true),
                    registration_required = table.Column<bool>(nullable: false),
                    update_contact_notes = table.Column<string>(type: "text", nullable: true),
                    url = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Brokers", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Contracts",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    bol_type = table.Column<string>(maxLength: 50, nullable: true),
                    broker_id = table.Column<int>(nullable: false),
                    dispatch_contact_notes = table.Column<string>(type: "text", nullable: true),
                    dispatch_date = table.Column<DateTime>(type: "date", nullable: false),
                    dropoff_address_city = table.Column<string>(maxLength: 50, nullable: true),
                    dropoff_address_country = table.Column<string>(maxLength: 50, nullable: true),
                    dropoff_address_line_1 = table.Column<string>(maxLength: 50, nullable: true),
                    dropoff_address_line_2 = table.Column<string>(maxLength: 50, nullable: true),
                    dropoff_address_state = table.Column<string>(maxLength: 25, nullable: true),
                    dropoff_address_zip = table.Column<string>(maxLength: 5, nullable: true),
                    dropoff_date = table.Column<DateTime>(type: "date", nullable: false),
                    dropoff_email = table.Column<string>(maxLength: 50, nullable: true),
                    dropoff_end = table.Column<DateTime>(type: "date", nullable: false),
                    dropoff_name = table.Column<string>(maxLength: 50, nullable: true),
                    dropoff_phone = table.Column<string>(maxLength: 50, nullable: true),
                    dropoff_start = table.Column<DateTime>(type: "date", nullable: false),
                    invoice_date = table.Column<DateTime>(type: "date", nullable: false),
                    make = table.Column<string>(maxLength: 50, nullable: true),
                    model = table.Column<string>(maxLength: 50, nullable: true),
                    order_number = table.Column<string>(maxLength: 50, nullable: true),
                    payment_date = table.Column<DateTime>(type: "date", nullable: false),
                    payment_method = table.Column<string>(maxLength: 50, nullable: true),
                    payment_terms = table.Column<int>(nullable: false),
                    pickup_address_city = table.Column<string>(maxLength: 50, nullable: true),
                    pickup_address_country = table.Column<string>(maxLength: 50, nullable: true),
                    pickup_address_line_1 = table.Column<string>(maxLength: 50, nullable: true),
                    pickup_address_line_2 = table.Column<string>(maxLength: 50, nullable: true),
                    pickup_address_state = table.Column<string>(maxLength: 25, nullable: true),
                    pickup_address_zip = table.Column<string>(maxLength: 5, nullable: true),
                    pickup_date = table.Column<DateTime>(type: "date", nullable: false),
                    pickup_email = table.Column<string>(maxLength: 50, nullable: true),
                    pickup_end = table.Column<DateTime>(type: "date", nullable: false),
                    pickup_name = table.Column<string>(maxLength: 50, nullable: true),
                    pickup_phone = table.Column<string>(maxLength: 50, nullable: true),
                    pickup_start = table.Column<DateTime>(type: "date", nullable: false),
                    price = table.Column<decimal>(nullable: false),
                    status = table.Column<int>(nullable: false),
                    trip_number = table.Column<int>(nullable: false),
                    update_contact_notes = table.Column<string>(type: "text", nullable: true),
                    vin_number = table.Column<string>(maxLength: 50, nullable: true),
                    year = table.Column<string>(maxLength: 4, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contracts", x => x.id);
                    table.ForeignKey(
                        name: "FK_Contracts_Brokers_broker_id",
                        column: x => x.broker_id,
                        principalTable: "Brokers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_broker_id",
                table: "Contracts",
                column: "broker_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contracts");

            migrationBuilder.DropTable(
                name: "Brokers");
        }
    }
}
