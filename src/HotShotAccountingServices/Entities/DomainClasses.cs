﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotShotAccountingServices.Entities
{
    public class Contract
    {

        [Key]
        public int id { get; set; }

        //basic info
        [Column(TypeName = "date")]
        public DateTime dispatch_date { get; set; }
        [StringLength(50)]
        public String order_number { get; set; }
        public Decimal price { get; set; }
        public Decimal broker_fee { get; set; }
        [Column(TypeName = "date")]
        public Nullable<DateTime> invoice_date { get; set; }
        [Column(TypeName = "date")]
        public Nullable<DateTime> payment_date { get; set; }
        public String status { get; set; }
        public bool rated { get; set; }
        public bool paid { get; set; }
        public Nullable<int> trip_number { get; set; }

        //broker related
        [Required]
        public int broker_id { get; set; }
        [ForeignKey("broker_id")]
        public Broker broker { get; set; }
        [StringLength(50)]
        public String bol_type { get; set; }
        [StringLength(50)]
        public String payment_method { get; set; }
        [Column(TypeName ="text")]
        public String dispatch_contact_notes { get; set; }
        [Column(TypeName = "text")]
        public String update_contact_notes { get; set; }
        public int payment_terms { get; set; }

        //vehicle info
        [StringLength(50)]
        public String vin_number { get; set; }
        [StringLength(4)]
        public String year { get; set; }
        [StringLength(50)]
        public String model { get; set; }
        [StringLength(50)]
        public String make { get; set; }

        //pickup
        [Column(TypeName = "text")]
        public String pickup_address { get; set; }

        [StringLength(50)]
        public String pickup_name { get; set; }
        [StringLength(50)]
        public String pickup_phone { get; set; }
        [StringLength(50)]
        public String pickup_email { get; set; }

        [Column(TypeName = "date")]
        public Nullable<DateTime> pickup_start { get; set; }
        [Column(TypeName = "date")]
        public Nullable<DateTime> pickup_end { get; set; }
        [Column(TypeName = "date")]
        public Nullable<DateTime> pickup_date { get; set; }

        //drop off
        [Column(TypeName = "text")]
        public String dropoff_address { get; set; }

        [StringLength(50)]
        public String dropoff_name { get; set; }
        [StringLength(50)]
        public String dropoff_phone { get; set; }
        [StringLength(50)]
        public String dropoff_email { get; set; }

        [Column(TypeName = "date")]
        public Nullable<DateTime> dropoff_start { get; set; }
        [Column(TypeName = "date")]
        public Nullable<DateTime> dropoff_end { get; set; }
        [Column(TypeName = "date")]
        public Nullable<DateTime> dropoff_date { get; set; }




    }


    public class Broker
    {
        [Key]
        public int id { get; set; }

        //basic info
        [Required,StringLength(100)]
        public String name { get; set; }
        [StringLength(100)]
        public String hours { get; set; }
        [StringLength(100)]
        public String url { get; set; }
        [StringLength(100)]
        public String load_board_url { get; set; }
        public String certificate_required { get; set; }
        [StringLength(50)]
        public String bol_type { get; set; }

        //payment
        public String extra_paperwork { get; set; }
        [StringLength(100)]
        public String payment_method { get; set; }

        //registration
        public String registration_required { get; set; }
        [StringLength(100)]
        public String login { get; set; }
        [StringLength(100)]
        public String password { get; set; }

        //contacts
        [Column(TypeName = "text")]
        public String dispatch_contact_notes { get; set; }
        [Column(TypeName = "text")]
        public String update_contact_notes { get; set; }

        [StringLength(50)]
        public String phone { get; set; }
        [StringLength(50)]
        public String fax { get; set; }
        [StringLength(50)]
        public String email { get; set; }


        //navigation
        public IList<Contract> contracts { get; set; }


    }


    public class ContractFormState
    {

        [Key]
        public int id { get; set; }

        //basic info
        [Column(TypeName = "date")]
        public DateTime dispatch_date { get; set; }
        [StringLength(50)]
        public String order_number { get; set; }
        public Decimal price { get; set; }
        public Decimal broker_fee { get; set; }
        [Column(TypeName = "date")]
        public Nullable<DateTime> invoice_date { get; set; }
        [Column(TypeName = "date")]
        public Nullable<DateTime> payment_date { get; set; }
        public String status { get; set; }
        public bool rated { get; set; }
        public bool paid { get; set; }
        public Nullable<int> trip_number { get; set; }


        //broker related
        public Nullable<int> broker_id { get; set; }
        public String broker_name { get; set; }
        [StringLength(50)]
        public String bol_type { get; set; }
        [StringLength(50)]
        public String payment_method { get; set; }
        [Column(TypeName = "text")]
        public String dispatch_contact_notes { get; set; }
        [Column(TypeName = "text")]
        public String update_contact_notes { get; set; }
        public int payment_terms { get; set; }

        //vehicle info
        [StringLength(50)]
        public String vin_number { get; set; }
        [StringLength(4)]
        public String year { get; set; }
        [StringLength(50)]
        public String model { get; set; }
        [StringLength(50)]
        public String make { get; set; }

        //pickup
        [Column(TypeName = "text")]
        public String pickup_address { get; set; }

        [StringLength(50)]
        public String pickup_name { get; set; }
        [StringLength(50)]
        public String pickup_phone { get; set; }
        [StringLength(50)]
        public String pickup_email { get; set; }

        [Column(TypeName = "date")]
        public Nullable<DateTime> pickup_start { get; set; }
        [Column(TypeName = "date")]
        public Nullable<DateTime> pickup_end { get; set; }

        //drop off
        [Column(TypeName = "text")]
        public String dropoff_address { get; set; }

        [StringLength(50)]
        public String dropoff_name { get; set; }
        [StringLength(50)]
        public String dropoff_phone { get; set; }
        [StringLength(50)]
        public String dropoff_email { get; set; }

        [Column(TypeName = "date")]
        public Nullable<DateTime> dropoff_start { get; set; }
        [Column(TypeName = "date")]
        public Nullable<DateTime> dropoff_end { get; set; }



        //form state
        public int active_section { get; set; }




    }








    public class BrokerFormState
    {
        [Key]
        public int id { get; set; }

        //basic info
        [StringLength(100)]
        public String name { get; set; }
        [StringLength(100)]
        public String hours { get; set; }
        [StringLength(100)]
        public String url { get; set; }
        [StringLength(100)]
        public String load_board_url { get; set; }
        public String certificate_required { get; set; }
        [StringLength(50)]
        public String bol_type { get; set; }

        //payment
        public String extra_paperwork { get; set; }
        [StringLength(100)]
        public String payment_method { get; set; }

        //registration
        public String registration_required { get; set; }
        [StringLength(100)]
        public String login { get; set; }
        [StringLength(100)]
        public String password { get; set; }

        //contacts
        [Column(TypeName = "text")]
        public String dispatch_contact_notes { get; set; }
        [Column(TypeName = "text")]
        public String update_contact_notes { get; set; }

        [StringLength(50)]
        public String phone { get; set; }
        [StringLength(50)]
        public String fax { get; set; }
        [StringLength(50)]
        public String email { get; set; }



        //form state
        public int active_section { get; set; }

    }

}
