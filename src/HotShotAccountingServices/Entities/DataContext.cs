﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace HotShotAccountingServices.Entities
{
    public class DataContext : DbContext
    {

        IConfigurationRoot _config;

        public DataContext(IConfigurationRoot config, DbContextOptions options) : base(options)
        {

            _config = config;

        }
        
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Broker> Brokers { get; set; }
        public DbSet<ContractFormState> ContractFormStates { get; set; }
        public DbSet<BrokerFormState> BrokerFormStates { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlServer(_config["db_connection:main"]);
        }

    }
}
