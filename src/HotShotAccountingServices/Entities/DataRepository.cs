﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotShotAccountingServices.Entities
{
    public class DataRepository
    {

        private DataContext _context;


        public DataRepository(DataContext context)
        {
            _context = context;
        }



        //broker retrieval
        public IEnumerable<Broker> GetAllBrokers()
        {
            return _context.Brokers.ToList();
        }

        //contract retrieval
        public IEnumerable<Contract> GetAllContracts()
        {
            return _context.Contracts.ToList();
        }

        //contract add/update/delete
        public int AddContract(Contract con)
        {

            _context.Contracts.Add(con);
            _context.SaveChanges();
            return con.id;

        }

        public Contract UpdateContract(Contract con)
        {
            Contract contract = _context.Contracts.Find(con.id);
            _context.Entry(contract).CurrentValues.SetValues(con);
            _context.SaveChanges();
            return con;

        }

        public Contract DeleteContract(Contract con)
        {

            Contract contract = _context.Contracts.Find(con.id);
            _context.Contracts.Remove(contract);
            _context.SaveChanges();

            return contract;
        }


        //broker add/update/delete
        public int AddBroker(Broker brok)
        {

            _context.Brokers.Add(brok);
            _context.SaveChanges();
            return brok.id;

        }

        public Broker UpdateBroker(Broker brok)
        {
            Broker broker = _context.Brokers.Find(brok.id);
            _context.Entry(broker).CurrentValues.SetValues(brok);
            _context.SaveChanges();
            return broker;

        }

       
        public Broker DeleteBroker(Broker con)
        {

            Broker broker = _context.Brokers.Find(con.id);
            _context.Brokers.Remove(broker);
            _context.SaveChanges();

            return broker;


        }



        //contract form state
        public ContractFormState GetContractFormState()
        {

            return _context.ContractFormStates.FirstOrDefault();


        }



        public ContractFormState UpdateContractFormState(ContractFormState con)
        {
            ContractFormState contractForm = _context.ContractFormStates.FirstOrDefault();

            if(contractForm == null)
            {
                _context.ContractFormStates.Add(con);
                _context.SaveChanges();
                return con;
                
            } else
            {
                _context.Entry(contractForm).CurrentValues.SetValues(con);
                _context.SaveChanges();
                return contractForm;
            }
            
        }



        //contract form state
        public BrokerFormState GetBrokerFormState()
        {

            return _context.BrokerFormStates.FirstOrDefault();


        }



        public BrokerFormState UpdateBrokerFormState(BrokerFormState con)
        {
            BrokerFormState brokerForm = _context.BrokerFormStates.FirstOrDefault();

            if (brokerForm == null)
            {
                _context.BrokerFormStates.Add(con);
                _context.SaveChanges();
                return con;

            }
            else
            {
                _context.Entry(brokerForm).CurrentValues.SetValues(con);
                _context.SaveChanges();
                return brokerForm;
            }

        }

    }
}
