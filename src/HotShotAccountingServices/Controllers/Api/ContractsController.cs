﻿using HotShotAccountingServices.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotShotAccountingServices.Controllers.Api
{
    [Route("api/contracts")]
    public class ContractsController : Controller
    {

        private DataRepository _repo;

        public ContractsController(DataRepository repo)
        {

            _repo = repo;

        }

        [HttpGet("all")]
        public IActionResult GetAllContracts()
        {

            var results = _repo.GetAllContracts();
            return Ok(results);

        }

        [HttpPost("add")]
        public IActionResult AddContract([FromBody]Contract con)
        {

            if (ModelState.IsValid)
            {
                var id = _repo.AddContract(con);

                return Created($"api/contracts/{id}",con);
            }

            return BadRequest(ModelState);

        }

        [HttpPost("update")]
        public IActionResult UpdateContract([FromBody]Contract con)
        {

            if (ModelState.IsValid)
            {
                Contract contract = _repo.UpdateContract(con);

                return Ok(contract);
            }

            return BadRequest(ModelState);

        }

        [HttpDelete("delete")]
        public IActionResult DeleteContract([FromBody]Contract con)
        {

            if (ModelState.IsValid)
            {
                Contract contract = _repo.DeleteContract(con);

                return Ok(contract);
            }

            return BadRequest(ModelState);


        }


    }
}
