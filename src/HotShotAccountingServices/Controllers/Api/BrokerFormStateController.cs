﻿using HotShotAccountingServices.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotShotAccountingServices.Controllers.Api
{
    [Route("api/brokerFormState")]
    public class BrokerFormStateController : Controller
    {

        private DataRepository _repo;

        public BrokerFormStateController(DataRepository repo)
        {

            _repo = repo;

        }


        [HttpGet("state")]
        public IActionResult GetBrokerFormState()
        {

            var results = _repo.GetBrokerFormState();
            return Ok(results);

        }


        [HttpPost("save")]
        public IActionResult UpdateBroker([FromBody]BrokerFormState con)
        {

            if (ModelState.IsValid)
            {
                BrokerFormState brokerForm = _repo.UpdateBrokerFormState(con);

                return Ok(brokerForm);
            }

            return BadRequest(ModelState);

        }


    }
}
