﻿using HotShotAccountingServices.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotShotAccountingServices.Controllers
{


    [Route("api/brokers")]
    public class BrokersController : Controller
    {

        private DataRepository _repo;

        public BrokersController(DataRepository repo)
        {

            _repo = repo;

        }

        [HttpGet("all")]
        public IActionResult GetAllBrokers()
        {

            var results = _repo.GetAllBrokers();
            return Ok(results);

        }

        [HttpPost("add")]
        public IActionResult AddBroker([FromBody]Broker brok)
        {

            if (ModelState.IsValid)
            {
                var id = _repo.AddBroker(brok);

                return Ok(brok);
            }

            return BadRequest(ModelState);

        }

        [HttpPost("update")]
        public IActionResult UpdateBroker([FromBody]Broker brok)
        {

            if (ModelState.IsValid)
            {
                Broker broker = _repo.UpdateBroker(brok);

                return Ok(broker);
            }

            return BadRequest(ModelState);

        }

        [HttpDelete("delete")]
        public IActionResult DeleteBroker([FromBody]Broker brok)
        {

            if (ModelState.IsValid)
            {
                Broker broker = _repo.DeleteBroker(brok);

                return Ok(broker);
            }

            return BadRequest(ModelState);


        }



    }
}
