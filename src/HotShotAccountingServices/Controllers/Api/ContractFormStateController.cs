﻿using HotShotAccountingServices.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotShotAccountingServices.Controllers.Api
{
    [Route("api/contractFormState")]
    public class ContractFormStateController : Controller
    {

        private DataRepository _repo;

        public ContractFormStateController(DataRepository repo)
        {

            _repo = repo;

        }


        [HttpGet("state")]
        public IActionResult GetContractFormState()
        {

            var results = _repo.GetContractFormState();
            return Ok(results);

        }
        

        [HttpPost("save")]
        public IActionResult UpdateContract([FromBody]ContractFormState con)
        {

            if (ModelState.IsValid)
            {
                ContractFormState contractForm = _repo.UpdateContractFormState(con);

                return Ok(contractForm);
            }

            return BadRequest(ModelState);

        }


    }
}
