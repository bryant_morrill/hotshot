﻿indexModule.factory('form_service', function () {


    return {

        nullify_empty_strings: function (object) {
            for (var propertyName in object) {
                if (object.hasOwnProperty(propertyName)) {
                    if (object[propertyName] === "") {
                        object[propertyName] = null;
                    }
                }
            }
        },

        to_slc_date: function (date) {

            var d = new Date(date);
            var s = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
            return s;

        },

        convert_date_slc: function (dates, object) {

            date_array = dates.split(',');
            for (var i = 0; i < date_array.length; i++) {

                object[date_array[i]] = object[date_array[i]] == null ? null : this.to_slc_date(object[date_array[i]]);
            }
        },


        convert_date_js: function (dates, object) {

            date_array = dates.split(',');

            for(var i = 0; i < date_array.length; i++){

                object[date_array[i]] = object[date_array[i]] == null ? null : new Date(object[date_array[i]]);
                
            }
        },

    }

        




})