﻿indexModule.factory('brokers_service', function ($http,$q,form_service) {

    var brokers_service = {};
    brokers_service.status = {};
    brokers_service.initialized = false;
    brokers_service.brokers = [];
    brokers_service.brokerFormState = {};

    brokers_service.initialize = function () {

        if (this.initialized) {

            return $q.when([]);

        } else {

            //get brokers
            var promise1 = $http.get("/api/brokers/all").then(function (response) {

                brokers_service.brokers = response.data;
                return response;
            }, function (error) {

            }).finally(function () {

            });


            //get form state
            var promise2 = $http.get("/api/brokerFormState/state").then(function (response) {

                if (response.status === 200) {

                    brokers_service.brokerFormState = response.data;
                    return response;
                }

            }, function (error) {

            }).finally(function () {

            });

            return $q.all([promise1, promise2]).then(function () {

                brokers_service.initialized = true;

            });


        }
    };


    brokers_service.isInitialized = function () {

        return this.initialized;

    };

    brokers_service.getBrokers = function () {

        return angular.copy(this.brokers);

    };

    brokers_service.getBrokerNames = function () {

        return _.pluck(this.brokers, 'name');

    };

    brokers_service.getBrokersByNames = function () {

        return _.reduce(this.brokers, function (array, broker) {

            array[broker.name] = broker;

            return array;

        }, []);

    };

    brokers_service.getBrokersByIds = function () {

        return _.reduce(this.brokers, function (array, broker) {

            array[broker.id] = broker;

            return array;

        }, []);

    };

    brokers_service.getPaymentMethods = function () {

        return _.reduce(this.brokers, function (array, broker) {

            if (broker.payment_method != null && !_.contains(array, broker.payment_method)) {

                array.push(broker.payment_method);

            }

            return array;

        }, []);

    };

    brokers_service.getBrokerFormState = function () {

        return angular.copy(this.brokerFormState);

    };

    brokers_service.addBrokerLocal = function (broker) {
        if (!this.initialized) return 1;
        if (broker != null && broker.id != null && broker.name != null && this.getBrokersByNames()[broker.name] == null) {
            this.brokers.push(broker);
            return 0;
        }
        return 1;
    };

    brokers_service.saveBrokerFormState = function (brokerState) {

        if (brokerState.active_section < 0) {
            return;
        }

        this.brokerFormState = angular.copy(brokerState);

        let formState = angular.copy(brokerState);

        form_service.nullify_empty_strings(formState);

        return $http.post("/api/brokerFormState/save", formState).then(function (response) {

            if (brokerState.id == null) {

                brokerState.id = response.data.id;
            }

            return response;

        }, function (error) {

        }).finally(function () {
            
        });

    };

    brokers_service.getNewBroker = function (id) {

        return { id: id, bol_type: 'Not Specified', certificate_required: 'Not Specified', extra_paperwork: 'Not Specified', registration_required: 'Not Specified', active_section: -1 }

    };

    brokers_service.addBroker = function (brokerState) {

        let final_broker = angular.copy(brokerState);

        form_service.nullify_empty_strings(final_broker);

        //deleting the id property to avoid problems on the back end
        delete final_broker.id;

        return $http.post("/api/brokers/add", final_broker).then(function (response) {

            final_broker.id = response.data.id;
            brokers_service.addBrokerLocal(final_broker);

            return response;

        }, function (error) {

        }).finally(function () { });

    };

    brokers_service.getBroker = function (id) {

        return _.find(this.brokers, function (b) {

            return b.id == id;

        });

    };

    brokers_service.saveBroker = function (brokerState) {

        let formState = angular.copy(brokerState);

        form_service.nullify_empty_strings(formState);

        return $http.post("/api/brokers/update", formState).then(function (response) {

            let updatedBroker = response.data;

            var i = _.findIndex(brokers_service.brokers, function (broker) { return broker.id == updatedBroker.id });

            brokers_service.brokers[i] = updatedBroker;

            return response;

        }, function (error) {

        }).finally(function () {
            
        });

    };

    brokers_service.deleteBroker = function (brokerState) {

        let formState = angular.copy(brokerState);

        form_service.nullify_empty_strings(formState);

        return $http({
            method: 'DELETE',
            url: "/api/brokers/delete",
            data: formState,
            headers: {
                'Content-type': 'application/json;charset=utf-8'
            }
        }).then(function (response) {

            let updatedBroker = response.data;

            var filter = function (broker) {

                if (broker.id == updatedBroker.id) {

                    return true;

                }

                return false;
            }

            brokers_service.brokers = _.reject(brokers_service.brokers, filter);

            return response;

        });

    };

    brokers_service.getBrokersByProperties = function (keyValuePairs) {

        return _.where(this.brokers, keyValuePairs);

    }

    brokers_service.getBrokersByFilter = function (filterFunc) {

        return _.filter(this.brokers, filterFunc);

    }

    return brokers_service;
})