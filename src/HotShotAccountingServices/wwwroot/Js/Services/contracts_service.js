﻿indexModule.factory('contracts_service', function ($http, $q, $location, form_service) {

    var contracts_service = {};
    contracts_service.status = {};
    contracts_service.initialized = 0;
    contracts_service.contracts = [];
    contracts_service.contractFormState = {};

    contracts_service.initialize = function () {

        if (this.initialized) {

            return $q.when([]);

        } else {

            //get brokers
            var promise1 = $http.get("/api/contracts/all").then(function (response) {

                contracts_service.contracts = response.data;
                return response;

            }, function (error) {

            }).finally(function () {

            });


            //get form state
            var promise2 = $http.get("/api/contractFormState/state").then(function (response) {

                if (response.status === 200) {

                    temp = response.data;

                    form_service.convert_date_js("dispatch_date,pickup_start,pickup_end,dropoff_start,dropoff_end", temp)

                    contracts_service.contractFormState = temp;
                } else if (response.status === 204) {

                    contracts_service.contractFormState = contracts_service.getNewContract();

                }

                return response;

            }, function (error) {

            }).finally(function () {

            });

            return $q.all([promise1, promise2]).then(function (response) {

                contracts_service.initialized = true;
                return response;
            })


        }
    };

    contracts_service.isInitialized = function () {

        return this.initialized == 2;

    };

    contracts_service.getContracts = function () {

        return angular.copy(this.contracts);

    };

    contracts_service.getPaymentMethods = function () {

        return _.reduce(this.contracts, function (memo, contract) {

            if (contract.payment_method != null && !_.contains(memo, contract.payment_method)) {
      
                memo.push(contract.payment_method);

            }

            return memo;

        }, []);

    };

    contracts_service.getContractFormState = function () {

        return angular.copy(this.contractFormState);

    };

    contracts_service.addContractLocal = function (contract) {
        if (this.initialized < 3) return 1;
        if (contract != null && contract.id != null) {
            this.contracts.push(contract);
            return 0;
        }
        return 1;
    };

    contracts_service.saveContractFormState = function (contractState,contractForm,status) {

        if (contractState.active_section < 0) {
            return;
        }

        this.contractFormState = angular.copy(contractState);

        let formState = angular.copy(contractState);

        form_service.nullify_empty_strings(formState);

        form_service.convert_date_slc("dispatch_date,pickup_start,pickup_end,dropoff_start,dropoff_end", formState)

        return $http.post("/api/contractFormState/save", formState).then(function (response) {

            if (contractState.id == null) {

                contractState.id = response.data.id;
            }
            return response;

        }, function (error) {

        }).finally(function () {
            
        });

    }

    contracts_service.getNewContract = function (id) {

        return {
            id: id,
            dispatch_date: new Date(),
            status: "Dispatched",
            broker_id: "-1",
            trip_id: "-1",
            active_section: 0
        };

    }
    
    contracts_service.addContract = function (status,contractState) {

            this.status = status;

            this.status.saving = true;

            let final_contract = angular.copy(contractState);

            form_service.nullify_empty_strings(final_contract);

            //deleting the id property to avoid problems on the back end
            delete final_contract.id;

            form_service.convert_date_slc("dispatch_date,pickup_start,pickup_end,dropoff_start,dropoff_end", final_contract);

            $http.post("/api/contracts/add", final_contract).then(function (response) {

                final_contract.id = response.data.id;
                console.log(final_contract.id);
                contracts_service.status.final_contract = final_contract;
                contracts_service.addContractLocal(final_contract);
                console.log(contracts_service.contracts);
                contracts_service.status.added = true;
                return response;

            }, function (error) {

                contracts_service.status.errorMessage = "Failed to create new contract: " + error;

            }).finally(function () { });

    }

    contracts_service.getContract = function (id) {

        for (var i = 0, len = this.contracts.length; i < len; i++){
            if (this.contracts[i].id == id) {
                var temp = angular.copy(this.contracts[i]);
                form_service.convert_date_js("dispatch_date,pickup_start,pickup_end,dropoff_start,dropoff_end", temp);
                return temp;
            };
        }

    };

    contracts_service.saveContract = function (contractState, contractForm, status) {

        let formState = angular.copy(contractState);
        
        form_service.nullify_empty_strings(formState);

        form_service.convert_date_slc("dispatch_date,pickup_start,pickup_end,dropoff_start,dropoff_end", formState)

        return $http.post("/api/contracts/update", formState).then(function (response) {

            let updatedContract = response.data;

            var i = _.findIndex(contracts_service.contracts, function () { return broker.id == updatedContract.id });

            contracts_service.contracts[i] = updatedContract;

            return response;

        }, function (error) {

        }).finally(function () {
            
        });

    }

    contracts_service.deleteContract = function (contractState) {

        let formState = angular.copy(contractState);

        form_service.nullify_empty_strings(formState);

        form_service.convert_date_slc("dispatch_date,pickup_start,pickup_end,dropoff_start,dropoff_end", formState)

        return $http({
            method: 'DELETE',
            url: "/api/contracts/delete",
            data: formState,
            headers: {
                'Content-type': 'application/json;charset=utf-8'
            }
        }).then(function (response) {

            let updatedContract = response.data;


            var filter = function (contract) {

                if(contract.id == updatedContract.id){
                
                    return true;

                }

                return false;
            }

            contracts_service.contracts = _.reject(contracts_service.contracts, filter);

            return response;

        });

    }

    contracts_service.getContractsByProperties = function (keyValuePairs) {

        return _.where(this.contracts, keyValuePairs);

    }

    contracts_service.getContractsByFilter = function (filterFunc) {

        return _.filter(this.contracts, filterFunc);

    }

    contracts_service.getContractsByFilterAndProperties = function (keyValuePairs, filterFunc) {

        return _.where(_.filter(this.contracts, filterFunc),keyValuePairs);

    }

    return contracts_service;
})