﻿angular.module("index").controller('newTripController', ['$scope','close','header','message',


    function ($scope, close, header,message) {


        $scope.header = header;
        $scope.message = message;
        $scope.closed = false;

        $scope.new_trip = {trip_number:0, origin:'', destination:'', notes:''};

        $scope.close = function (result) {

            if (result) {

                var valid = true;

                //validation here
                if ($scope.new_trip.trip_number.length == 0) {
                    valid = false;
                }

                if ($scope.new_trip.origin.length == 0) {
                    valid = false;
                }

                if ($scope.new_trip.destination.length == 0) {
                    valid = false;
                }

                if ($scope.new_trip.notes.length == 0) {
                    valid = false;
                }

                if (valid) {



                    var wrapper = {result:result,trip:$scope.new_trip};

                    $scope.closed = true;
                    close(wrapper, 500); // close, but give 500ms for bootstrap to animate
                
                } else {

                    //tell user form is invalid

                }

            } else {

                var wrapper = { result: result};
                $scope.closed = true;
                close(wrapper, 500); // close, but give 500ms for bootstrap to animate


            }

            
        };

        this.close = $scope.close;

}]);