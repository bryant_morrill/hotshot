﻿(function () {

    "use strict";

    angular.module("index")
        .controller("brokersController", brokersController);


    function brokersController($scope, $timeout, $routeParams, $location, $q, contracts_service, brokers_service) {

        $scope.status = { loading: true};
        $scope.header = "Brokers";
        $scope.tableLoaded = false;
        $scope.columns = {};
        $scope.properties = $location.search();
        $scope.tableContents = [];

        var promise1 = contracts_service.initialize();
        var promise2 = brokers_service.initialize();

        $q.all([promise1, promise2]).then(function () {

            var temp = brokers_service.getBrokers();

            if ($location.hash() == "Search") {

                $scope.subHeader = "Search";
                temp = brokers_service.getBrokersByProperties($scope.properties);

            } else if ($location.hash() == "Filter") {

                $scope.subHeader = "Search";

                var filter = function (broker) {

                    var accept = true;

                    Object.keys($scope.properties).forEach(function (key, index) {

                        if (broker[key] == $scope.properties[key]) {
                            accept = false;
                        }

                    });

                    return accept;

                };

                temp = brokers_service.getBrokersByFilter(filter);

            }

            setColumns($location.hash());

            for (var i = 0; i < temp.length; i++) {

                var id = temp[i].id;
                var filter = function (contract) {

                    return (contract.broker_id == id && contract.status != "Closed");

                };

                var contracts = contracts_service.getContractsByFilter(filter);

                temp[i].contract_count = contracts.length;

                var total = _.reduce(contracts, function (sum, contract) {

                    if (!contract.paid) {
                        sum = sum + contract.price;
                    }

                    return sum;

                }, 0.0);

                temp[i].outstanding_dollars = total;


            }

            $scope.tableContents = temp;
            $scope.status.loading = false;

        });

        //initialization

        //event handlers and functions


        //var deRegisterLoadWatch = $scope.$watch("status.loading", function () {
        //    if ($scope.status.loading == 0 && contracts_service.isInitialized() && brokers_service.isInitialized()) {

        //        var temp = brokers_service.getBrokers();

        //        if ($location.hash() == "Search") {

        //            $scope.subHeader = "Search";
        //            temp = brokers_service.getBrokersByProperties($scope.properties);

        //        } else if ($location.hash() == "Filter") {

        //            $scope.subHeader = "Search";

        //            var filter = function (broker) {

        //                var accept = true;

        //                Object.keys($scope.properties).forEach(function (key, index) {

        //                    if (broker[key] == $scope.properties[key]) {
        //                        accept = false;
        //                    }

        //                });

        //                return accept;
                        
        //            };

        //            temp = brokers_service.getBrokersByFilter(filter);

        //        }

        //        setColumns($location.hash());

        //        for (var i = 0; i < temp.length; i++ ){

        //            var id = temp[i].id;
        //            var filter = function (contract) {

        //                return (contract.broker_id == id && contract.status != "Closed");

        //            };

        //            var contracts = contracts_service.getContractsByFilter(filter);

        //            temp[i].contract_count = contracts.length;

        //            var total = _.reduce(contracts, function (sum, contract) {

        //                if (!contract.paid) {
        //                    sum = sum + contract.price;
        //                }

        //                return sum;

        //            }, 0.0);

        //            temp[i].outstanding_dollars = total;


        //        }
                
        //        $scope.tableContents = temp;

        //        animateToState(0);
        //        deRegisterLoadWatch();
        //    }

        //});


        $scope.formatDate = function(date){

            var parts = date.split("T");
            var dateParts = parts[0].split("-");

            return dateParts[1] + "/" + dateParts[2] + "/" + dateParts[0];
 

        }

        $scope.dtInstance = function (inst) {

            $scope.dtInstance = inst;
            $scope.tableLoaded = true;

        };

        $scope.search = function (input, index) {
            $scope.dtInstance.DataTable.column(index).search(angular.element('#search_' + input).val()).draw();

        };

        $scope.toggleColumn = function (column) {

            $scope.columns[column] = !$scope.columns[column];
            if ($scope.columns[column]) {
                $scope.colCount++
            } else {

                $scope.colCount--;
            }

        }

        $scope.resetColumns = function (column) {

            setColumns($location.hash());

        }

        $scope.navigate = function (path, search, hash) {

            $location.path(path).search(search).hash(hash);

        }

        var setColumns = function (hash) {

            $scope.columns = { name: true, phone: true, email: true, contract_count: true, outstanding_dollars: true, url: false, load_board_url: false, bol_type: false };

        }




    }

})();