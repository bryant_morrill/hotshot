﻿angular.module("index").controller('confirmController', ['$scope', 'close','header','message',


    function ($scope, close, header,message) {


        $scope.header = header;
        $scope.message = message;
        $scope.closed = false;

        $scope.close = function (result) {
            $scope.closed = true;
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

        this.close = $scope.close;

}]);