﻿(function () {

    "use strict";

    angular.module("index")
        .controller("newContractController", newContractController);


    function newContractController($http, $scope, $timeout, $location, $q, form_service, contracts_service, brokers_service) {

        $scope.status = { loading: false, formStateError: null, formStateMessage: null, saving: true, editMode: true };


        var promise1 = contracts_service.initialize();
        var promise2 = brokers_service.initialize();

        $q.all([promise1, promise2]).then(function () {

            $("#payment_method").autocomplete({
                source: contracts_service.getPaymentMethods()
            }).bind('focus', function () { });

            $("#broker_name").autocomplete({
                source: brokers_service.getBrokerNames()

            }).bind('focus', function () { });

            var temp = contracts_service.getContractFormState();
            let rampUpTo = temp.active_section;
            temp.active_section = -1;

            //change name in case the broker name was edited
            if (temp.broker_id >= 0 && brokers_service.getBrokersByIds()[temp.broker_id] != null) {
                temp.broker_name = brokers_service.getBrokersByIds()[temp.broker_id].name;
            }

            $scope.new_contract = temp;

            $scope.checkbroker();
            //show and animate form
            $scope.status.loading = false;
            animateToState(rampUpTo);
            //enable the form to be saved.
            $scope.status.saving = false;
            

        });

        //initialization
        $scope.new_contract = contracts_service.getNewContract(null);

        let saveTimeout = null;
        var animateToState = function (rampUpTo) {

            $timeout(function () {

                if ($scope.new_contract.active_section < rampUpTo) {
                    $scope.new_contract.active_section = rampUpTo;
                }
            }, 100);

        }

        //event handlers and functions

        //next
        $scope.next = function () {
            $scope.new_contract.active_section++;
        };

        //previous
        $scope.previous = function () {
            $scope.new_contract.active_section--;
        };

        $scope.clear = function () {
            $scope.new_contract = contracts_service.getNewContract($scope.new_contract.id);
            saveForm();
            $scope.checkbroker();
            animateToState(0);
        };

        $scope.navigate = function (path, search, hash) {

            $location.path(path).search(search).hash(hash);

        }

        $scope.checkbroker = function () {

            if (typeof (brokers_service.getBrokersByNames()[$scope.new_contract.broker_name]) === "undefined") {
                $scope.new_contract.broker_id = -1;
                if ($scope.new_contract.broker_name != null && $scope.new_contract.broker_name.length > 0) {
                    $scope.new_contract.broker_id = -2;
                }

            } else {

                let temp = brokers_service.getBrokersByNames()[$scope.new_contract.broker_name];

                $scope.new_contract.broker_id = temp.id;
                $scope.new_contract.bol_type = temp.bol_type;
                $scope.new_contract.payment_method = temp.payment_method;
                $scope.new_contract.dispatch_contact_notes = temp.dispatch_contact_notes;
                $scope.new_contract.update_contact_notes = temp.update_contact_notes;

            }

        }

        $scope.addContract = function () {

            $scope.status.loading = true;

            contracts_service.addContract($scope.status).then(function (result) {

                $location.path("edit_contract/" + response.data.id).replace();

            }, function (error) {

                $scope.status.errorMessage = "Failed to create new contract: " + error;
                

            }).finally(function () {

                $scope.status.loading = false;

            });;;

        }


        $scope.$watchCollection("new_contract", function (newValue, oldValue) {

            if ($scope.status.saving || newValue === oldValue) {
                return;
            }

            if (saveTimeout) {
                $scope.status.formStateMessage = "Changes not saved.";
                $timeout.cancel(saveTimeout);
            }

            saveTimeout = $timeout(function () {
                saveForm();

            }, 500)


        });


        var saveForm = function(){
        
            $scope.status.saving = true;
            $scope.status.formStateMessage = "Saving...";

            contracts_service.saveContractFormState($scope.new_contract).then(function (result) {

                $scope.status.formStateMessage = "Form saved." + ($scope.new_contract.broker_id >= 0 && $scope.new_contract_form.$valid ? "" : " Form not valid.");
                $scope.status.formStateError = null;

            }, function (error) {

                $scope.status.formStateMessage = "Changes not saved." + ($scope.new_contract.broker_id >= 0 && $scope.new_contract_form.$valid ? "" : " Form not valid.");
                $scope.status.formStateError = "Failed to save changes: " + error;

            }).finally(function () {

                $scope.status.saving = false;

            });;
        
        }

    }

})();