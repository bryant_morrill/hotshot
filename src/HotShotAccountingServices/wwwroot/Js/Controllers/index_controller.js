﻿(function () {

    "use strict";

    angular.module("index")
        .controller("indexController", indexController);


    function indexController($scope, $location) {

        

        $scope.navigate = function (path, search, hash) {

            $location.path(path).search(search).hash(hash);

        }
    }

})();