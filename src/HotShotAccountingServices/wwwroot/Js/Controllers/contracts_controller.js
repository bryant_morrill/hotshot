﻿(function () {

    "use strict";

    angular.module("index")
        .controller("contractsController", contractsController);


    function contractsController($scope, $timeout, $location, $routeParams,$q, DTColumnBuilder, contracts_service, brokers_service, form_service) {

        $scope.status = { loading: true};
        $scope.header = "Contracts";
        $scope.tableLoaded = false;
        $scope.columns = {};
        $scope.properties = $location.search();
        $scope.tableContents = [];

        var promise1 = contracts_service.initialize();
        var promise2 = brokers_service.initialize();

        $q.all([promise1, promise2]).then(function () {

            var temp;

            if ($location.hash() == "Dispatched" || $location.hash() == "Picked-up" || $location.hash() == "Delivered" || $location.hash() == "Invoiced" || $location.hash() == "Closed") {

                $scope.subHeader = "Status: " + $location.hash();
                temp = contracts_service.getContractsByProperties($scope.properties);

            } else if ($location.hash() == "Broker") {

                $scope.subHeader = "Broker: " + brokers_service.getBrokersByIds()[$scope.properties.broker_id].name;

                var filter = function (contract) {

                    var accept = true;

                    if (contract.broker_id != $scope.properties.broker_id || contract.status == "Closed" || contract.deleted) {
                        accept = false;
                    }

                    return accept;

                };

                temp = contracts_service.getContractsByFilter(filter);

            } else if ($location.hash() == "BrokerAll") {

                $scope.subHeader = "Broker: " + brokers_service.getBrokersByIds()[$scope.properties.broker_id].name;

                var filter = function (contract) {

                    var accept = true;

                    if (contract.broker_id != $scope.properties.broker_id || contract.deleted) {
                        accept = false;
                    }

                    return accept;

                };

                temp = contracts_service.getContractsByFilter(filter);

            } else if ($location.hash() == "BrokerClosed") {

                $scope.subHeader = "Broker: " + brokers_service.getBrokersByIds()[$scope.properties.broker_id].name;

                var filter = function (contract) {

                    var accept = true;

                    if (contract.broker_id != $scope.properties.broker_id || contract.status != "Closed" || contract.deleted) {
                        accept = false;
                    }

                    return accept;

                };

                temp = contracts_service.getContractsByFilter(filter);

            } else if ($location.hash() == "Search") {

                temp = contracts_service.getContractsByProperties($scope.properties);

            } else if ($location.hash() == "Filter") {

                var filter = function (contract) {

                    var accept = true;

                    Object.keys($scope.properties).forEach(function (key, index) {

                        if (contract[key] == $scope.properties[key]) {
                            accept = false;
                        }
                    });

                    return accept;

                };


                temp = contracts_service.getContractsByFilter(filter);

            } else {

                status.errorMessage = "Bad query parameters."

            }

            setColumns($location.hash());

            for (var i = 0; i < temp.length; i++) {

                var id = temp[i].broker_id;
                var broker_name = brokers_service.getBrokersByIds()[id].name;
                temp[i].broker_name = broker_name;

            }

            $scope.tableContents = temp;
            $scope.status.loading = false;

        });

        //initialization

        //event handlers and functions

        //var deRegisterLoadWatch = $scope.$watch("status.loading", function () {
        //    if ($scope.status.loading == 0 && contracts_service.isInitialized() && brokers_service.isInitialized()) {

        //        var temp;

        //        if ($location.hash() == "Dispatched" || $location.hash() == "Picked-up" || $location.hash() == "Delivered" || $location.hash() == "Invoiced" || $location.hash() == "Closed") {

        //            $scope.subHeader = "Status: " + $location.hash();
        //            temp = contracts_service.getContractsByProperties($scope.properties);

        //        } else if ($location.hash() == "Broker") {

        //            $scope.subHeader = "Broker: " + brokers_service.getBrokersByIds()[$scope.properties.broker_id].name;

        //            var filter = function (contract) {

        //                var accept = true;

        //                if (contract.broker_id != $scope.properties.broker_id || contract.status == "Closed" || contract.deleted) {
        //                    accept = false;
        //                }

        //                return accept;

        //            };

        //            temp = contracts_service.getContractsByFilter(filter);

        //        } else if ($location.hash() == "BrokerAll") {

        //            $scope.subHeader = "Broker: " + brokers_service.getBrokersByIds()[$scope.properties.broker_id].name;

        //            var filter = function (contract) {

        //                var accept = true;

        //                if (contract.broker_id != $scope.properties.broker_id || contract.deleted) {
        //                    accept = false;
        //                }

        //                return accept;

        //            };

        //            temp = contracts_service.getContractsByFilter(filter);

        //        } else if ($location.hash() == "BrokerClosed") {

        //            $scope.subHeader = "Broker: " + brokers_service.getBrokersByIds()[$scope.properties.broker_id].name;

        //            var filter = function (contract) {

        //                var accept = true;

        //                if (contract.broker_id != $scope.properties.broker_id || contract.status != "Closed" || contract.deleted) {
        //                    accept = false;
        //                }

        //                return accept;

        //            };

        //            temp = contracts_service.getContractsByFilter(filter);

        //        } else if ($location.hash() == "Search") {

        //            temp = contracts_service.getContractsByProperties($scope.properties);

        //        } else if ($location.hash() == "Filter") {

        //            var filter = function (contract) {

        //                var accept = true;

        //                Object.keys($scope.properties).forEach(function (key, index) {

        //                    if (contract[key] == $scope.properties[key]) {
        //                        accept = false;
        //                    }
        //                });

        //                return accept;
                        
        //            };


        //            temp = contracts_service.getContractsByFilter(filter);

        //        } else {

        //            status.errorMessage = "Bad query parameters."

        //        }

        //        setColumns($location.hash());

        //        for (var i = 0; i < temp.length; i++) {

        //            var id = temp[i].broker_id;
        //            var broker_name = brokers_service.getBrokersByIds()[id].name;
        //            temp[i].broker_name = broker_name;

        //        }

        //        $scope.tableContents = temp;

        //        deRegisterLoadWatch();
        //    }

        //});

        $scope.dtInstance = function (inst) {

            $scope.dtInstance = inst;
            $scope.tableLoaded = true;

        };


        $scope.search = function (input, index) {
            $scope.dtInstance.DataTable.column(index).search(angular.element('#search_'+input).val()).draw();

        };

        $scope.formatDate = function(date){

            if (date == null) return "";

            var parts = date.split("T");
            var dateParts = parts[0].split("-");

            return dateParts[1] + "/" + dateParts[2] + "/" + dateParts[0];
 

        }


        $scope.toggleColumn = function (column) {

            $scope.columns[column] = !$scope.columns[column];

        }

        $scope.resetColumns = function (column) {

            setColumns($location.hash());

        }

        var setColumns = function (hash) {

            $scope.columns = { trip_number: true, broker: true, vehicle: true, price: true, status: true, paid: true };

            $scope.columns.dispatch_date = false;
            $scope.columns.estimated_pickup = false;
            $scope.columns.pickup_date = false;
            $scope.columns.estimated_dropoff = false;
            $scope.columns.dropoff_date = false;
            $scope.columns.invoice_date = false;
            $scope.columns.payment_terms = false;
            $scope.columns.payment_date = false;
            $scope.columns.order_number = false;
            $scope.columns.vin = false;

            if (hash == "Dispatched") {

                $scope.columns.dispatch_date = true;
                $scope.columns.estimated_pickup = true;

            } else if (hash == "Picked-up") {

                $scope.columns.pickup_date = true;
                $scope.columns.estimated_dropoff = true;

            } else if (hash == "Delivered") {

                $scope.columns.dropoff_date = true;

            } else if (hash == "Invoiced") {

                $scope.columns.invoice_date = true;
                $scope.columns.payment_terms = true;
                $scope.columns.payment_date = true;
                
            } else {

                $scope.columns.dispatch_date = true;
                $scope.columns.estimated_pickup = true;
                $scope.columns.pickup_date = true;
                $scope.columns.estimated_dropoff = true;
                $scope.columns.dropoff_date = true;
                $scope.columns.invoice_date = true;
                $scope.columns.payment_terms = true;
                $scope.columns.payment_date = true;
            }

        }



    }

})();