﻿(function () {

    "use strict";

    angular.module("index")
        .controller("newBrokerController", newBrokerController);


    function newBrokerController($http, $scope, $timeout, $location, form_service, brokers_service) {

        $scope.status = { loading: true, formStateError: null, formStateMessage: null, saving: true, brokernamevalid: 0, editMode: true };

        brokers_service.initialize().then(function () {

            $("#payment_method").autocomplete({
                source: brokers_service.getPaymentMethods()
            }).bind('focus', function () { });

            var temp = brokers_service.getBrokerFormState();
            if (temp.active_section < 0) {
                temp.active_section = 0;
            }
            let rampUpTo = temp.active_section;
            temp.active_section = -1;
            $scope.new_broker = temp;

            $scope.checkbrokername();

            //enable the form to be saved.
            $scope.status.saving = false;
            $scope.status.loading = false;
            animateToState(rampUpTo);


        });

        //initialization
        $scope.new_broker = brokers_service.getNewBroker(null)
        let saveTimeout = null;
        var animateToState = function(rampUpTo){

            $timeout(function () {

            if ($scope.new_broker.active_section < rampUpTo) {
                $scope.new_broker.active_section = rampUpTo;
            }
            }, 100);
        }

        //event handlers and functions

        //next
        $scope.next = function () {
            $scope.new_broker.active_section++;
        };

        //previous
        $scope.previous = function () {
            $scope.new_broker.active_section--;
        };

        //reset form
        $scope.clear = function () {
            $scope.new_broker = brokers_service.getNewBroker($scope.new_broker.id);
            saveForm();
            $scope.checkbrokername();
            animateToState(0);

        };

        //make sure a broker with same name doesn't already exist
        $scope.checkbrokername = function () {

            if ($scope.new_broker.name != null && $scope.new_broker.name.length > 0) {


                if (typeof (brokers_service.getBrokersByNames()[$scope.new_broker.name]) === "undefined") {

                    $scope.status.brokernamevalid = 0;
                } else {
                    $scope.status.brokernamevalid = -2;;

                }


            } else {
                $scope.status.brokernamevalid = -1;
            }

        }

        //add broker
        $scope.addBroker = function () {

            $scope.status.loading = true;

            brokers_service.addBroker($scope.new_broker).then(function (response) {

                $location.path("edit_broker/" + response.data.id).replace();

            }, function (error) {

                $scope.status.errorMessage = "Failed to create new broker: " + error;

            }).finally(function () {

                $scope.status.loading = false;

            });;

        }

        //watch form and save when there are changes after a minor delay
        $scope.$watchCollection("new_broker", function (newValue, oldValue) {

            if ($scope.status.saving || newValue === oldValue) {
                return;
            }

            if (saveTimeout) {
                $scope.status.formStateMessage = "Changes not saved.";
                $timeout.cancel(saveTimeout);
            }

            saveTimeout = $timeout(function () {
                saveForm();

            }, 500)


        });


        var saveForm = function(){

            $scope.status.saving = true;
            $scope.status.formStateMessage = "Saving...";

            brokers_service.saveBrokerFormState($scope.new_broker).then(function (result) {

                $scope.status.formStateMessage = "Form saved." + ($scope.status.brokernamevalid == 0 && $scope.new_broker_form.$valid ? "" : " Form not valid.");
                $scope.status.formStateError = null;

            }, function (error) {

                $scope.status.formStateMessage = "Changes not saved." + ($scope.status.brokernamevalid == 0 && $scope.new_broker_form.$valid ? "" : " Form not valid.");
                $scope.status.formStateError = "Failed to save changes: " + error;

            }).finally(function () {

                $scope.status.saving = false;

            });

        }

    }

})();