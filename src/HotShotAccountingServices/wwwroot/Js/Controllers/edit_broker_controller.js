﻿(function () {

    "use strict";

    angular.module("index")
        .controller("editBrokerController", editBrokerController);


    function editBrokerController($scope, $timeout, $routeParams, $location, $q, ModalService, form_service, brokers_service, contracts_service) {

        $scope.status = { loading: true, formStateError: null, formStateMessage: null, saving: true, brokernamevalid: 0, loaded: false, editMode: false};
        
        //initialization

        var promise1 = contracts_service.initialize();
        var promise2 = brokers_service.initialize();

        $q.all([promise1, promise2]).then(function () {

            $("#payment_method").autocomplete({
                source: brokers_service.getPaymentMethods()
            }).bind('focus', function () { });

            $scope.edit_broker = brokers_service.getBroker($routeParams.id);


            var filter = function (contract) {

                return (contract.broker_id == $scope.edit_broker.id);

            };

            $scope.contract_count_all = contracts_service.getContractsByFilter(filter).length;

            $scope.status.formStateMessage = "Broker successfully loaded."
            
            //show and animate form
            $scope.status.loading = false;
            animateToState(0);
            //enable the form to be saved.
            $scope.status.saving = false;

        });

        
        let saveTimeout = null;
        $scope.active_section = -1;

        //event handlers and functions

        var animateToState = function (rampUpTo) {

            $timeout(function () {

                if ($scope.active_section < rampUpTo) {
                    $scope.active_section = rampUpTo;
                }
            }, 100);
        }

        //edite
        $scope.edit = function () {
            $scope.status.editMode = true;
        };

        //done
        $scope.done = function () {
            $scope.status.editMode = false;
        };

        $scope.selectTab = function (tab) {

            $scope.active_section = tab;

        };

        $scope.deleteIt = function () {

            if ($scope.contract_count_all > 0) {

                ModalService.showModal({
                    templateUrl: "/Views/OkayModal.html",
                    controller: "okayController",
                    inputs: {
                        header: "Cannot Delete Broker",
                        message: "Only brokers that don't have any contracts associated with them can be deleted (this includes closed contracts). This broker has " + $scope.contract_count_all + " contracts associated with it. Click the link below to view them.",
                        linkText: "View Contracts",
                        showLink: true
                    }
                }).then(function (modal) {

                    modal.element.modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    modal.close.then(function (result) {

                        if(result){
                            $scope.navigate('contracts', { broker_id: $scope.edit_broker.id }, 'BrokerAll');
                  
                        }

                    });
                });
            
            } else {

                ModalService.showModal({
                    templateUrl: "/Views/ConfirmModal.html",
                    controller: "confirmController",
                    inputs: {
                        header: "Delete Broker",
                        message: "Are you sure you want to permanently delete this broker?"
                    }
                }).then(function (modal) {

                    modal.element.modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    modal.close.then(function (result) {
                        if (result) {
                            $scope.status.saving = true;
                            $scope.status.formStateMessage = "Deleting...";

                            brokers_service.deleteBroker($scope.edit_broker).then(function () {

                                $scope.status.formStateMessage = "Broker deleted.";
                                $scope.status.formStateError = null;

                                ModalService.showModal({
                                    templateUrl: "/Views/OkayModal.html",
                                    controller: "okayController",
                                    inputs: {
                                        header: "Broker Deleted",
                                        message: "The broker was deleted.",
                                        linkText: "",
                                        showLink: false
                                    }
                                }).then(function (modal) {
                                    //modal.element.on('hidden.bs.modal', function () {
                                    //    if (!modal.controller.closed) {
                                    //        modal.controller.close(false);
                                    //    }
                                    //});
                                    modal.element.modal({
                                        backdrop: 'static',
                                        keyboard: false
                                    });
                                    modal.close.then(function () {

                                        $location.path('/').replace();

                                    });
                                });

                            }, function (error) {

                                $scope.status.formStateMessage = "Contract not deleted."
                                $scope.status.formStateError = "Failed to delete contract: " + error;

                            }).finally(function () {
                                $scope.status.saving = false;
                            });

                        }
                    });
                });
            }
        };


        //make sure a broker with same name doesn't already exist
        $scope.checkbrokername = function () {

            if ($scope.edit_broker.name != null && $scope.edit_broker.name.length > 0) {


                if (typeof (brokers_service.getBrokersByNames()[$scope.edit_broker.name]) === "undefined" || brokers_service.getBrokersByNames()[$scope.edit_broker.name].id == $scope.edit_broker.id) {

                    $scope.status.brokernamevalid = 0;
                } else {
                    $scope.status.brokernamevalid = -2;;

                }


            } else {
                $scope.status.brokernamevalid = -1;
            }

        }


        $scope.navigate = function (path, search, hash) {

            $location.path(path).search(search).hash(hash);

        }

        //watch form and save when there are changes after a minor delay
        $scope.$watchCollection("edit_broker", function (newValue, oldValue) {

            if ($scope.status.saving || newValue === oldValue) {
                return;
            }

            if (saveTimeout) {
                $scope.status.formStateMessage = "Changes not saved.";
                $timeout.cancel(saveTimeout);
            }

            saveTimeout = $timeout(function () {
                if ($scope.edit_broker_form.$valid && $scope.status.brokernamevalid == 0) {

                    $scope.status.saving = true;
                    $scope.status.formStateMessage = "Saving...";

                    brokers_service.saveBroker($scope.edit_broker).then(function (result) {

                        $scope.status.formStateMessage = "Broker Saved";
                        $scope.status.formStateError = null;

                    }, function (error) {

                        $scope.status.formStateMessage = "Broker not saved."
                        $scope.status.formStateError = "Failed to save changes: " + error;

                    }).finally(function () {

                        $scope.status.saving = false;

                    });

                } else {

                    $scope.status.formStateMessage = "Changes not saved. Invalid entry.";
                }

            }, 500)


        });

        $scope.status.loading--;

    }

})();