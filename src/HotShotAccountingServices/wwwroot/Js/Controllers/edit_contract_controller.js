﻿(function () {

    "use strict";

    angular.module("index")
        .controller("editContractController", newContractController);


    function newContractController($scope, $timeout, $routeParams, $location, ModalService, form_service, contracts_service, brokers_service) {

        $scope.status = { loading: false, formStateError: null, formStateMessage: null, saving: true, editMode: false };


        var promise1 = contracts_service.initialize();
        var promise2 = brokers_service.initialize();

        $q.all([promise1, promise2]).then(function () {

            $("#payment_method").autocomplete({
                source: contracts_service.getPaymentMethods()
            }).bind('focus', function () { });

            $("#broker_name").autocomplete({
                source: brokers_service.getBrokerNames()

            }).bind('focus', function () { });

            $scope.edit_contract = contracts_service.getContract($routeParams.id)

            $scope.status.formStateMessage = "Contract successfully loaded."

            $scope.edit_contract.broker_name = brokers_service.getBrokersByIds()[$scope.edit_contract.broker_id].name;

            $scope.checkbroker();

            
            //show and animate form
            $scope.status.loading = false;
            animateToState(0);
            //enable the form to be saved.
            $scope.status.saving = false;

        });

        //initialization

        let saveTimeout = null;
        $scope.active_section = -1;

        //event handlers and functions

        var animateToState = function (rampUpTo) {

            $timeout(function () {

                if ($scope.active_section < rampUpTo) {
                    $scope.active_section = rampUpTo;
                }
            }, 100);
        }

        $scope.selectTab = function (tab) {

            $scope.active_section = tab;

        };

        //edite
        $scope.edit = function () {
            $scope.status.editMode = true;
        };

        //done
        $scope.done = function () {
            $scope.status.editMode = false;
        };

        $scope.deleteIt = function () {

            ModalService.showModal({
                templateUrl: "/Views/ConfirmModal.html",
                controller: "confirmController",
                inputs: {
                    header: "Delete Contract",
                    message: "Are you sure you want to permanently delete this contract?"
                }
            }).then(function (modal) {
                modal.element.modal({
                    backdrop: 'static',
                    keyboard: false
                });
                modal.close.then(function (result) {
                    if (result) {

                        $scope.status.saving = true;
                        $scope.status.formStateMessage = "Deleting...";

                        contracts_service.deleteContract($scope.edit_contract).then(function () {

                            $scope.status.formStateMessage = "Contract deleted.";
                            $scope.status.formStateError = null;

                            ModalService.showModal({
                                templateUrl: "/Views/OkayModal.html",
                                controller: "okayController",
                                inputs: {
                                    header: "Contract Deleted",
                                    message: "The contract was deleted.",
                                    linkText: "",
                                    showLink: false
                                }
                            }).then(function (modal) {
                                modal.element.modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });
                                modal.close.then(function () {

                                    $location.path('/').replace();

                                });
                            });

                        }, function (error) {

                            $scope.status.formStateMessage = "Contract not deleted."
                            $scope.status.formStateError = "Failed to delete contract: " + error;

                        }).finally(function () {
                            $scope.status.saving = false;
                        });

                    }
                });
            });

        };

        $scope.checkbroker = function () {

            if (typeof (brokers_service.getBrokersByNames()[$scope.edit_contract.broker_name]) === "undefined") {
                $scope.edit_contract.broker_id = -1;
                if ($scope.edit_contract.broker_name != null && $scope.edit_contract.broker_name.length > 0) {
                    $scope.edit_contract.broker_id = -2;
                }

            } else {
                $scope.edit_contract.broker_id = brokers_service.getBrokersByNames()[$scope.edit_contract.broker_name].id;

            }

        }

        $scope.navigate = function (path, search, hash) {

            $location.path(path).search(search).hash(hash);

        }

        $scope.$watchCollection("edit_contract", function (newValue, oldValue) {

            if ($scope.status.saving || newValue === oldValue) {
                return;
            }

            if (saveTimeout) {
                $scope.status.formStateMessage = "Changes not saved.";
                $timeout.cancel(saveTimeout);
            }

            saveTimeout = $timeout(function () {
                if ($scope.edit_contract_form.$valid && $scope.edit_contract.broker_id >= 0) {

                    $scope.status.saving = true;
                    $scope.status.formStateMessage = "Saving...";

                    contracts_service.saveContract($scope.edit_contract).then(function (result) {

                        $scope.status.formStateMessage = "Contract Saved";
                        $scope.status.formStateError = null;

                    }, function (error) {

                        $scope.status.formStateMessage = "Contract not saved."
                        $scope.status.formStateError = "Failed to save changes: " + error;

                    }).finally(function () {

                        $scope.status.saving = false;

                    });;
                } else {

                    $scope.status.formStateMessage = "Changes not saved. Invalid entry.";
                }
            }, 500)


        });

    }

})();