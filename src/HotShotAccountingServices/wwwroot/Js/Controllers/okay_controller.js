﻿angular.module("index").controller('okayController', ['$scope', 'header', 'message', 'linkText', 'showLink', 'close',


    function ($scope, header, message, linkText, showLink, close) {


        $scope.header = header;
        $scope.message = message;
        $scope.linkText = linkText;
        $scope.showLink = showLink;
        $scope.closed = false;

        

        $scope.close = function (result) {
            $scope.closed = true;
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

        this.close = $scope.close;


        

    }]);