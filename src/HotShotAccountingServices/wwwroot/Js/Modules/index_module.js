﻿var indexModule;

(function () {


    indexModule = angular.module("index", ["ngRoute", "datatables", "angularModalService"])
        .config(function ($routeProvider) {

            $routeProvider.when("/", {
                controller: "dashboardController",
                templateUrl: "/Views/Dashboard.html"
            });

            $routeProvider.when("/new_contract", {
                controller: "newContractController",
                templateUrl: "/Views/NewContract.html"
            });

            $routeProvider.when("/new_broker", {
                controller: "newBrokerController",
                templateUrl: "/Views/NewBroker.html"
            });

            $routeProvider.when("/edit_broker/:id", {
                controller: "editBrokerController",
                templateUrl: "/Views/EditBroker.html"
            });

            $routeProvider.when("/edit_contract/:id", {
                controller: "editContractController",
                templateUrl: "/Views/EditContract.html"
            });

            $routeProvider.when("/contracts", {
                controller: "contractsController",
                templateUrl: "/Views/Contracts.html"
            });

            $routeProvider.when("/brokers", {
                controller: "brokersController",
                templateUrl: "/Views/Brokers.html"
            });

            $routeProvider.otherwise({ redirectTo: "/" });


        });


})();