﻿indexModule.directive('brokerContactFieldset', function () {

    return {

        templateUrl: "/Views/Templates/BrokerContactFieldset.html",
        scope: {

            broker: "=",
            broker_form: "=",
            next: "&",
            previous: "&",
            edit: "&",
            done: "&",
            deleteIt: "&",
            editMode: "=",
            formStateError: "=",
            formStateMessage: "=",
            saving: "=",
            clear: "&",
            newBrokerForm: "="


        }

    };


});