﻿indexModule.directive('brokerBasicFieldset', function () {

    return {

        templateUrl: "/Views/Templates/BrokerBasicFieldset.html",
        scope: {

            broker: "=",
            broker_form: "=",
            next: "&",
            previous: "&",
            navigate:"&",
            edit: "&",
            done: "&",
            deleteIt: "&",
            editMode: "=",
            formStateError: "=",
            formStateMessage: "=",
            saving: "=",
            clear: "&",
            newBrokerForm: "=",
            checkbrokername: "&",
            brokernamevalid: "=",
            contract_count_open: "=",
            contract_count_closed: "="


        }

    };


});