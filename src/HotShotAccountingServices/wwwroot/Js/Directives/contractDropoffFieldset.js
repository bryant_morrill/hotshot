﻿indexModule.directive('contractDropoffFieldset', function () {

    return {

        templateUrl: "/Views/Templates/ContractDropoffFieldset.html",
        scope: {

            contract: "=",
            contract_form: "=",
            next: "&",
            previous: "&",
            edit: "&",
            deleteIt: "&",
            done: "&",
            editMode: "=",
            formStateError: "=",
            formStateMessage: "=",
            saving: "=",
            clear: "&",
            newContractForm: "="


        }

    }


});