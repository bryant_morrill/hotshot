﻿indexModule.directive('contractVehicleFieldset', function () {

    return {

        templateUrl: "/Views/Templates/ContractVehicleFieldset.html",
        scope: {

            contract: "=",
            contract_form: "=",
            next: "&",
            previous: "&",
            edit: "&",
            done: "&",
            deleteIt: "&",
            editMode: "=",
            formStateError: "=",
            formStateMessage: "=",
            saving: "=",
            clear: "&",
            newContractForm: "="


        }

    }


});