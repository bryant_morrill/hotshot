﻿indexModule.directive('brokerOnlineFieldset', function () {

    return {

        templateUrl: "/Views/Templates/BrokerOnlineFieldset.html",
        scope: {

            broker: "=",
            broker_form: "=",
            next: "&",
            previous: "&",
            edit: "&",
            done: "&",
            deleteIt: "&",
            editMode: "=",
            formStateError: "=",
            formStateMessage: "=",
            saving: "=",
            clear: "&",
            newBrokerForm: "="


        }

    };


});