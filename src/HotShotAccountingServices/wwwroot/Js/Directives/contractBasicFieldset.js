﻿indexModule.directive('contractBasicFieldset', function () {

    return {

        templateUrl: "/Views/Templates/ContractBasicFieldset.html",
        scope: {

            contract: "=",
            contract_form: "=",
            next: "&",
            previous: "&",
            edit: "&",
            done: "&",
            checktrip: "&",
            createtrip: "&",
            currenttrip: "&",
            deleteIt: "&",
            editMode: "=",
            formStateError: "=",
            formStateMessage: "=",
            saving: "=",
            clear: "&",
            newContractForm: "="


        }

    }


});