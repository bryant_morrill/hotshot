﻿indexModule.directive('contractPickupFieldset', function () {

    return {

        templateUrl: "/Views/Templates/ContractPickupFieldset.html",
        scope: {

            contract: "=",
            contract_form: "=",
            next: "&",
            previous: "&",
            formStateError: "=",
            edit: "&",
            done: "&",
            deleteIt: "&",
            editMode: "=",
            formStateMessage: "=",
            saving: "=",
            clear: "&",
            newContractForm: "="


        }

    }


});