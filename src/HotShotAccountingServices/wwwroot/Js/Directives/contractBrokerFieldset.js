﻿indexModule.directive('contractBrokerFieldset', function () {

    return {

        templateUrl: "/Views/Templates/ContractBrokerFieldset.html",
        scope: {

            contract: "=",
            contract_form: "=",
            next: "&",
            previous: "&",
            edit: "&",
            done: "&",
            deleteIt: "&",
            navigate: "&",
            editMode: "=",
            formStateError: "=",
            formStateMessage: "=",
            saving: "=",
            clear: "&",
            newContractForm: "=",
            checkbroker: "&"

        }

    }


});